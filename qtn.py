#! /usr/bin/env python
# -*- coding: latin-1 -*-

"""
Python module to build Cassini/RPWS/HFR/QTN PDS4 data bundle

cassini-rpws-hfr-qtn-archive-pds4
Copyright (C) 2018  Cecconi Baptiste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from spacepy import pycdf
from scipy.io import readsav
from pathlib import Path
import sys
import datetime
import numpy
import math
import os
import subprocess
import hashlib
from astropy import time as atime
import configparser

__author__ = "Baptiste Cecconi"
__copyright__ = "Copyright 2018, LESIA-PADC-USN, Observatoire de Paris"
__credits__ = ["Baptiste Cecconi"]
__license__ = "GPLv3"
__version__ = "1.0rc2"
__maintainer__ = "Baptiste Cecconi"
__email__ = "baptiste.cecconi@obspm.fr"
__status__ = "Development"
__date__ = "05-OCT-2018"
__project__ = "MASER/KRONOS"

# Setup the local path directory
_CUR_PATH = Path(__file__).absolute().parent

# Loading configuration from config.ini file
config = configparser.ConfigParser()
config.read('config.ini')

# Define the version numbers below.
_VERSION_SFT = config['versions']['sft_version'].replace('.', '')
_VERSION_CDF = config['versions']['cdf_version'].replace('.', '')
_VERSION_SAV = config['versions']['sav_version'].replace('.', '')

# Setup the local data and bundle directories
_SAVDATA_DIR = Path(config['directories']['data_dir'])
_BUILD_DIR = _CUR_PATH / 'build'

# Setup the library path for CDF lib
if 'cdflib_bin' in config['libraries'].keys():
    _CDFLIB_BIN = Path(config['libraries']['cdflib_bin'])
else:
    _CDFLIB_BIN = Path(os.environ['CDF_BIN'])

# Setup the library path for IGPP docgen
if 'docgen' in config['libraries'].keys():
    _DOCGEN_BIN = Path(config['libraries']['docgen'])
else:
    _DOCGEN_BIN = _CUR_PATH / Path("lib/igpp-docgen/bin/docgen")

# Setup the library path for PDS PPI CDF validator
if 'pds_cdf' in config['libraries'].keys():
    _CDFCHECK_BIN = Path(config['libraries']['pds_cdf'])
else:
    _CDFCHECK_BIN = _CUR_PATH / Path('lib/pds-cdf/bin/cdfcheck')

# Setup the library path for PDS PPI CDF validator
if 'validate' in config['libraries'].keys():
    _VALIDATE_BIN = Path(config['libraries']['validate'])
else:
    _VALIDATE_BIN = _CUR_PATH / Path('lib/validate/bin/validate')

# Setup the library path for SKTEditor ISTP validator
if 'skteditor' in config['libraries'].keys():
    _SKTEDITOR_BIN = Path(config['libraries']['validate'])
else:
    _SKTEDITOR_BIN = _CUR_PATH / Path('lib/skteditor/spdfjavaClasses.jar')

# Setup the library path for PDS PPI CDF validator
if 'java_home' in config['libraries'].keys():
    _JAVA_HOME = Path(config['libraries']['java_home'])
else:
    # https://stackoverflow.com/questions/5136611/capture-stdout-from-a-script-in-python/5136686#comment41883876_5136686
    _JAVA_HOME = Path(subprocess.check_output("/bin/bash -c 'echo $(/usr/libexec/java_home )'",
                                              shell=True).decode('ascii').strip())

_BUNDLE_NAME = "co_rpws_saturn_hfr_qtn_bundle_v{0}".format(_VERSION_SFT)
_BUNDLE_DIR = _BUILD_DIR / _BUNDLE_NAME

_DATA_COLL_DIR = _BUNDLE_DIR / 'hfr-qtn-data'
_DATA_CDF_DIR = _DATA_COLL_DIR / 'cdf'
_DATA_COLL_INVENTORY_FILE = _DATA_COLL_DIR / 'collection_co_rpws_hfr_qtn_data.csv'

_BROWSE_COLL_DIR = _BUNDLE_DIR / 'hfr-qtn-browse'
_BROWSE_PLOT_DIR = _BROWSE_COLL_DIR / 'plot'
_BROWSE_THUMBNAIL_DIR = _BROWSE_COLL_DIR / 'thumbnail'
_BROWSE_COLL_INVENTORY_FILE = _BROWSE_COLL_DIR / 'collection_co_rpws_hfr_qtn_browse.csv'

_DOCUMENT_COLL_DIR = _BUNDLE_DIR / 'hfr-qtn-document'
_DOCUMENT_DIR = _DOCUMENT_COLL_DIR / 'files'
_DOCUMENT_COLL_INVENTORY_FILE = _DOCUMENT_COLL_DIR / 'collection_co_rpws_hfr_qtn_document.csv'

_EXTRA_DIR = _CUR_PATH / "extra"
_README_FILE = _EXTRA_DIR / "Readme_1.0.txt"


def clean():
    """
    This function cleans the build directory
    """

    cur_command = ' '.join(["rm", "-rf", "{}/[a-zA-Z]*".format(str(_BUILD_DIR))])
    os.system(cur_command)


def import_initial_data():
    """
    This function loads the initial data (IDL savesets), precomputed plots and thumbnails.
    """

    check_dirs()

    initial_data_root_url = "http://www.lesia.obspm.fr/kronos/data/qtn/"
    log_tmp = ""

    data_file_dir = {'idlsav': _BUILD_DIR / "idlsav",
                     'plots': _BROWSE_PLOT_DIR,
                     'thumbs': _BROWSE_THUMBNAIL_DIR}

    for cur_file, cur_dir in data_file_dir.items():
        if not cur_dir.exists():
            cur_dir.mkdir()

        cur_command = ' '.join(["cd", "/tmp", "&&",
                                "wget", "{}{}.tgz".format(initial_data_root_url, cur_file), "&&",
                                "tar", "-xvzf", "{}.tgz".format(str(cur_file)), "&&",
                                "cp", "-v", "/tmp/{}/*.*".format(cur_file), "{}/.".format(str(cur_dir)), "&&",
                                "rm", "-rf", "/tmp/{}*".format(cur_file), "&&",
                                "cd", str(_CUR_PATH)])
        os.system(cur_command)
        log_tmp += "Running: {}\n".format(cur_command)

    cur_command = ' '.join(["cp", str(_README_FILE), str(_BUNDLE_DIR)])
    os.system(cur_command)
    log_tmp += "Running: {}\n".format(cur_command)

    return log_tmp


def get_data_file_list(file_type='cdf') -> list:
    """
    This function returns the list of files, for the specified file format. Allowed file formats: 'sav' for raw data IDL
    savesets; 'cdf' for processed CDF files.
    :param file_type: 'sav or 'cdf'
    :return: a list of file paths.
    """

    if file_type == 'sav':
        file_dir = _SAVDATA_DIR
    elif file_type == 'cdf':
        file_dir = _DATA_CDF_DIR
    else:
        raise Exception('Wrong file_type (ust be "cdf or "sav")')

    return list(file_dir.glob('*.{}'.format(file_type)))


def check_dirs():
    """
    This function checks (and create if necessary) the bundle directory structure
    :return: nothing :-)
    """
    if not _BUNDLE_DIR.exists():
        _BUNDLE_DIR.mkdir()
    if not _DATA_COLL_DIR.exists():
        _DATA_COLL_DIR.mkdir()
    if not _BROWSE_COLL_DIR.exists():
        _BROWSE_COLL_DIR.mkdir()
    if not _DATA_CDF_DIR.exists():
        _DATA_CDF_DIR.mkdir()
    if not _DOCUMENT_COLL_DIR.exists():
        _DOCUMENT_COLL_DIR.mkdir()
    file_doc_dir = _DOCUMENT_COLL_DIR / 'files'
    if not file_doc_dir.exists():
        file_doc_dir.mkdir()
    if not _BROWSE_PLOT_DIR.exists():
        _BROWSE_PLOT_DIR.mkdir()
    if not _BROWSE_THUMBNAIL_DIR.exists():
        _BROWSE_THUMBNAIL_DIR.mkdir()


class QTNData(object):
    """
    The QTNData class defines the generic data class for QTN data. It is initialized from an IDL Saveset file.

    Attributes:
        - `data` contains the data extracted from the IDL saveset file.
        - `time` contains the time of each record
        - `rev_id` contains the Rev ID name (Revolution number) of the current orbit.
        - `start_time` and `end_time` contain the start and end time of the data.
        - `time_resolution`contains the average time resolution for the data

    Methods:
        - `__len__` provides the number of record (use len() on the class instance).
        - `make_cdf` launches the CDF conversion
        - `check_cdf` launches the CDF validation scripts
    """

    def __init__(self, filesav):
        self.file = filesav
        self.data = self._load_data()
        self._fix_nan_values()
        self._remove_empty_values()
        self.time = self._extract_datetime()
        self.rev_id = str(filesav.name)[12:15]
        self.start_time = self.time[0]
        self.end_time = self.time[-1]
        self.time_resolution = self._get_average_time_resolution()

    def __len__(self):
        return len(self.data['tempsjd_nsweep_v2_orbit'])

    def _load_data(self):
        return readsav(str(self.file))

    def _fix_nan_values(self):
        # replacing NaN values by CDF ISTP REAL4 FillVal = -1e+31
        for var in self.data:
            self.data[var] = [-1e31 if math.isnan(x) else x for x in self.data[var]]

    def _remove_empty_values(self):
        # removing all data with NE and TE at FillVal
        mask = numpy.logical_not([(mn and mt) for (mn, mt) in zip(
            numpy.array(self.data['NE_CORE_CC_CLEANED_ORBIT']) == -1e31,
            numpy.array(self.data['TE_CORE_EV_CLEANED_ORBIT']) == -1e31
        )])
        for var in self.data:
            self.data[var] = numpy.array(self.data[var])[mask]

    def _extract_datetime(self):
        return atime.Time(self.data['tempsjd_nsweep_v2_orbit'], format='jd').to_datetime()

    def _get_time_steps(self):
        dt = self.time[1:] - self.time[0:-1]
        time_step = []
        for cur_item in dt:
            time_step.append(cur_item.total_seconds())
        return time_step

    def _get_average_time_resolution(self):
        return numpy.average(self._get_time_steps())

    def _get_min_time_resolution(self):
        return numpy.min(self._get_time_steps())

    def _get_max_time_resolution(self):
        return numpy.max(self._get_time_steps())

    def _get_mission_phase(self):
        mission_phase = 'Tour>Prime Mission'
        if self.start_time >= datetime.datetime(2008, 7, 1):
            mission_phase = 'Extended Mission>Equinox Mission'
        if self.start_time >= datetime.datetime(2010, 10, 12):
            mission_phase = 'Extended-Extended Mission>Solstice Mission'
        if self.start_time >= datetime.datetime(2016, 11, 1):
            mission_phase = 'Extended-Extended Mission>Proximal Orbits'
        return mission_phase

    def _get_ymdhm_start(self):
        return datetime.datetime.strftime(self.start_time, "%Y%m%d%H%M")

    def _get_ymdhm_end(self):
        return datetime.datetime.strftime(self.end_time, "%Y%m%d%H%M")

    def _cdf_name(self):
        return 'co_rpws_hfr_qtn_{}_{}_v{}.cdf'.format(self._get_ymdhm_start(),
                                                      self._get_ymdhm_end(), _VERSION_CDF)

    def _cdf_label_name(self):
        return 'co_rpws_hfr_qtn_{}_{}_v{}.xml'.format(self._get_ymdhm_start(),
                                                      self._get_ymdhm_end(), _VERSION_CDF)

    def _pdf_name(self):
        return 'co_rpws_hfr_qtn_{}_{}_v{}.pdf'.format(self._get_ymdhm_start(),
                                                           self._get_ymdhm_end(), _VERSION_CDF)

    def _pdf_label_name(self):
        return 'co_rpws_hfr_qtn_{}_{}_v{}-plot.xml'.format(self._get_ymdhm_start(),
                                                           self._get_ymdhm_end(), _VERSION_CDF)

    def _png_name(self):
        return 'co_rpws_hfr_qtn_{}_{}_v{}-thumbnail.png'.format(self._get_ymdhm_start(),
                                                                self._get_ymdhm_end(), _VERSION_CDF)

    def _png_label_name(self):
        return 'co_rpws_hfr_qtn_{}_{}_v{}-thumbnail.xml'.format(self._get_ymdhm_start(),
                                                                self._get_ymdhm_end(), _VERSION_CDF)

    def make_cdf(self):

        cdf_file = _DATA_CDF_DIR / self._cdf_name()

        # removing existing CDF file with same name
        if cdf_file.exists():
            cdf_file.unlink()

        # Opening CDF object
        pycdf.lib.set_backward(False)  # this is setting the CDF version to be used

        cdf = pycdf.CDF(str(cdf_file), '')
        cdf.col_major(True)  # Column Major
        cdf.compress(pycdf.const.NO_COMPRESSION)  # No file level compression

        # Writing ISTP global attributes
        cdf.attrs["Project"] = ["PADC>Paris Astronomical Data Centre",
                                "CDPP>Plasma Physics Data Centre",
                                "PDS-PPI>Planetary Plasma Interaction Node of NASA Planetary Data System"]
        cdf.attrs['Discipline'] = "Planetary Physics>Particles"
        cdf.attrs['Data_type'] = 'HFR_QTN'
        cdf.attrs['Descriptor'] = 'RPWS'
        cdf.attrs['Data_version'] = _VERSION_SAV
        cdf.attrs['Instrument_type'] = 'Radio and Plasma Waves (space)'
        cdf.attrs['Logical_file_id'] = 'co_rpws_hfr_qtn_{}_{}_v{}'.format(self._get_ymdhm_start(),
                                                                          self._get_ymdhm_end(),
                                                                          _VERSION_CDF)
        cdf.attrs['Logical_source'] = 'co_rpws_hfr_qtn'
        cdf.attrs['Logical_source_description'] = 'Quasi Thermal Noise Spectroscopy on High Frequency Receiver Data'
        cdf.attrs['File_naming_convention'] = 'source_descriptor_type_yyyyMMddHHmm_yyyyMMddHHmm_ver'
        cdf.attrs['Mission_group'] = 'Cassini-Huygens'
        cdf.attrs['PI_name'] = "D.A. Gurnett"
        cdf.attrs['PI_affiliation'] = 'University of Iowa'
        cdf.attrs['Source_name'] = 'CO>Cassini Orbiter'
        cdf.attrs['TEXT'] = 'Quasi Thermal Noise Spectroscopy derived from Cassini RPWS HFR Data (Kurth et al., CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0, NASA PDS, 2004)'
        cdf.attrs['Generated_by'] = ['PADC/LESIA', 'CDPP']
        cdf.attrs['Generation_date'] = datetime.datetime.now().isoformat()
        cdf.attrs['LINK_TEXT'] = ["More details on ", "CDPP archive"]
        cdf.attrs['LINK_TITLE'] = ["LESIA Cassini Kronos webpage", "web site"]
        cdf.attrs['HTTP_LINK'] = ["http://www.lesia.obspm.fr/kronos", "https://cdpp-archive.cnes.fr"]
        cdf.attrs['MODS'] = " "
        cdf.attrs['Parents'] = self.file.name
        cdf.attrs['Rules_of_use'] = " "
        cdf.attrs['Skeleton_version'] = _VERSION_CDF
        cdf.attrs['Software_version'] = _VERSION_SFT
        cdf.attrs['Time_resolution'] = "{:5.2f} Seconds".format(self.time_resolution)
        cdf.attrs['Acknowledgement'] = " "
        cdf.attrs['ADID_ref'] = " "
        cdf.attrs['Validate'] = " "

        # Writing PDS/PPI global attributes

        cdf.attrs['PDS_orbit_number'] = self.rev_id
        cdf.attrs['PDS_mission_phase'] = self._get_mission_phase()
        cdf.attrs['PDS_start_time'] = self.start_time.isoformat()[:-3]+'Z'
        cdf.attrs['PDS_stop_time'] = self.end_time.isoformat()[:-3]+'Z'
        cdf.attrs['PDS_observation_target'] = "Saturn"
        cdf.attrs['PDS_observation_type'] = "Particles"
        cdf.attrs['PDS_collection_id'] = "urn:nasa:pds:co-rpws-saturn:hfr-qtn-data"
        cdf.attrs['PDS_LID'] = "urn:nasa:pds:co-rpws-saturn:hfr-qtn-data:{}-{}-cdf".\
            format(self._get_ymdhm_start(), self._get_ymdhm_end())
        cdf.attrs['PDS_LID_plot'] = "urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:{}-{}-plot".\
            format(self._get_ymdhm_start(), self._get_ymdhm_end())
        cdf.attrs['PDS_LID_thumbnail'] = "urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:{}-{}-thumbnail".\
            format(self._get_ymdhm_start(), self._get_ymdhm_end())
        cdf.attrs['PDS_LID_index'] = "urn:nasa:pds:co-rpws-saturn:hfr-qtn-document:index"
        cdf.attrs['PDS_RDR_data_set_id'] = "CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0"

        # Writing extra global attributes

        cdf.attrs['file_plot_name'] = self._pdf_name()
        cdf.attrs['file_thumbnail_name'] = self._png_name()

        # Writing VESPA global attributes

        cdf.attrs['VESPA_dataproduct_type'] = "TS>Time Series"
        cdf.attrs['VESPA_target_class'] = 'planet'
        cdf.attrs['VESPA_time_sampling_min'] = str(self._get_min_time_resolution())
        cdf.attrs['VESPA_time_sampling_max'] = str(self._get_max_time_resolution())
        cdf.attrs['VESPA_c1min'] = str(numpy.min(self.data['R_EPHEM_NSWEEP_ORBIT']))
        cdf.attrs['VESPA_c1max'] = str(numpy.max(self.data['R_EPHEM_NSWEEP_ORBIT']))
        cdf.attrs['VESPA_c2min'] = str(90 - numpy.max(self.data['LAT_EPHEM_NSWEEP_ORBIT']))
        cdf.attrs['VESPA_c2max'] = str(90 - numpy.min(self.data['LAT_EPHEM_NSWEEP_ORBIT']))
        cdf.attrs['VESPA_c3min'] = str(numpy.min(self.data['LT_EPHEM_NSWEEP_ORBIT']))
        cdf.attrs['VESPA_c3max'] = str(numpy.max(self.data['LT_EPHEM_NSWEEP_ORBIT']))
        cdf.attrs['VESPA_spatial_frame_type'] = 'spherical'
        cdf.attrs['VESPA_spatial_frame_description'] = 'SSQ>Saturn Solar Equatorial'
        cdf.attrs['VESPA_instrument_name'] = "RPWS>Radio and Plasma Waves Science"
        cdf.attrs['VESPA_receiver_name'] = "HFR>High Frequency Receiver"
        cdf.attrs['VESPA_measurement_type'] = ["phys.density;phys.electron", "phys.temperature;phys.electron"]
        cdf.attrs['VESPA_spatial_frame_axis_name'] = ['Radial Distance', "Colatitude", "Local Time"]
        cdf.attrs['VESPA_spatial_frame_axis_units'] = ['Rs>Saturn Radii', "deg", "hour"]

        # Writing SPASE Global Attributes

        cdf.attrs['spase_DatasetResourceID'] = 'spase://VSPO/NumericalData/Cassini/RPWS/QTN/PT32S'

        # SETTING UP VARIABLES AND VARIABLE ATTRIBUTES
        #   The EPOCH variable type must be CDF_TIME_TT2000
        #   PDS-CDF requires no compression for variables.
        cdf.new('EPOCH', data=self.time, type=pycdf.const.CDF_TIME_TT2000, compress=pycdf.const.NO_COMPRESSION)
        cdf['EPOCH'].attrs.new('VALIDMIN', data=datetime.datetime(2004, 6, 1), type=pycdf.const.CDF_TIME_TT2000)
        cdf['EPOCH'].attrs.new('VALIDMAX', data=datetime.datetime(2017, 9, 30), type=pycdf.const.CDF_TIME_TT2000)
        cdf['EPOCH'].attrs.new('SCALEMIN', data=self.start_time.replace(microsecond=0,
                                                                        second=0,
                                                                        minute=0,
                                                                        hour=(self.start_time.hour // 3)*3),
                               type=pycdf.const.CDF_TIME_TT2000)
        cdf['EPOCH'].attrs.new('SCALEMAX',
                               data=self.end_time.replace(microsecond=0, second=0, minute=0,
                                                          hour=(self.end_time.hour // 3)*3)+datetime.timedelta(hours=3),
                               type=pycdf.const.CDF_TIME_TT2000)
        cdf['EPOCH'].attrs['CATDESC'] = "Default time (TT2000)"
        cdf['EPOCH'].attrs['FIELDNAM'] = "Epoch"
        cdf['EPOCH'].attrs.new('FILLVAL', data=-9223372036854775808, type=pycdf.const.CDF_TIME_TT2000)
        cdf['EPOCH'].attrs['LABLAXIS'] = "Epoch"
        cdf['EPOCH'].attrs['UNITS'] = "ns"
        cdf['EPOCH'].attrs['FORM_PTR'] = "CDF_TIME_TT2000"
        cdf['EPOCH'].attrs['VAR_TYPE'] = "support_data"
        cdf['EPOCH'].attrs['SCALETYP'] = "linear"
        cdf['EPOCH'].attrs['MONOTON'] = "INCREASE"
        cdf['EPOCH'].attrs['REFERENCE_POSITION'] = "Spacecraft barycenter"
        cdf['EPOCH'].attrs['SI_CONVERSION'] = "1.0e-9>s"
        cdf['EPOCH'].attrs['UCD'] = "time.epoch"
        cdf['EPOCH'].attrs['TIME_BASE'] = 'UTC'

        # # Writing NE_CORE variable

        cdf.new('NE_CORE', data=self.data['NE_CORE_CC_CLEANED_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['NE_CORE'].attrs['CATDESC'] = "Core Electron Density"
        cdf['NE_CORE'].attrs['DEPEND_0'] = "EPOCH"
        cdf['NE_CORE'].attrs['DICT_KEY'] = "density>number"
        cdf['NE_CORE'].attrs['DISPLAY_TYPE'] = "time_series"
        cdf['NE_CORE'].attrs['FIELDNAM'] = 'NE_CORE'
        cdf['NE_CORE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['NE_CORE'].attrs['FORMAT'] = "E12.2"
        cdf['NE_CORE'].attrs['LABLAXIS'] = 'Core Electron Density'
        cdf['NE_CORE'].attrs['UNITS'] = "cm**-3"
        cdf['NE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['NE_CORE'].attrs.new('VALIDMAX', data=1e6, type=pycdf.const.CDF_REAL4)
        cdf['NE_CORE'].attrs['VAR_TYPE'] = "data"
        cdf['NE_CORE'].attrs['SCALETYP'] = "log"
        cdf['NE_CORE'].attrs.new('SCALEMIN', data=0.1, type=pycdf.const.CDF_REAL4)
        cdf['NE_CORE'].attrs.new('SCALEMAX', data=200, type=pycdf.const.CDF_REAL4)
        cdf['NE_CORE'].attrs['SI_CONVERSION'] = "1.0e-6>m**-3"
        cdf['NE_CORE'].attrs['UCD'] = "phys.density;phys.electron"
        cdf['NE_CORE'].attrs['DELTA_PLUS_VAR'] = 'D_NE_CORE'
        cdf['NE_CORE'].attrs['DELTA_MINUS_VAR'] = 'D_NE_CORE'

        # Writing TE_CORE variable

        cdf.new('TE_CORE', data=self.data['TE_CORE_EV_CLEANED_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['TE_CORE'].attrs['CATDESC'] = "Core Electron Temperature"
        cdf['TE_CORE'].attrs['DEPEND_0'] = "EPOCH"
        cdf['TE_CORE'].attrs['DICT_KEY'] = "temperature"
        cdf['TE_CORE'].attrs['DISPLAY_TYPE'] = "time_series"
        cdf['TE_CORE'].attrs['FIELDNAM'] = 'TE_CORE'
        cdf['TE_CORE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['TE_CORE'].attrs['FORMAT'] = "E12.2"
        cdf['TE_CORE'].attrs['LABLAXIS'] = 'Core Electron Temperature'
        cdf['TE_CORE'].attrs['UNITS'] = "eV"
        cdf['TE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['TE_CORE'].attrs.new('VALIDMAX', data=1e6, type=pycdf.const.CDF_REAL4)
        cdf['TE_CORE'].attrs['VAR_TYPE'] = "data"
        cdf['TE_CORE'].attrs['SCALETYP'] = "linear"
        cdf['TE_CORE'].attrs.new('SCALEMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['TE_CORE'].attrs.new('SCALEMAX', data=20, type=pycdf.const.CDF_REAL4)
        cdf['TE_CORE'].attrs['UCD'] = "phys.temperature;phys.electron"
        cdf['TE_CORE'].attrs['SI_CONVERSION'] = "11604.5250061657>K"
        cdf['TE_CORE'].attrs['DELTA_PLUS_VAR'] = 'D_TE_CORE'
        cdf['TE_CORE'].attrs['DELTA_MINUS_VAR'] = 'D_TE_CORE'

        # Writing D_NE_CORE variable

        cdf.new('D_NE_CORE', data=self.data['DELTA_NE_CORE_CC_CLEANED_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['D_NE_CORE'].attrs["CATDESC"] = "Uncertainty of electron density"
        cdf['D_NE_CORE'].attrs["DEPEND_0"] = "EPOCH"
        cdf['D_NE_CORE'].attrs["DICT_KEY"] = "uncertainty>density>number"
        cdf['D_NE_CORE'].attrs["DISPLAY_TYPE"] = "time_series"
        cdf['D_NE_CORE'].attrs["FIELDNAM"] = "D_NE_CORE"
        cdf['D_NE_CORE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['D_NE_CORE'].attrs['FORMAT'] = "E12.2"
        cdf['D_NE_CORE'].attrs["LABLAXIS"] = "Error on electron density"
        cdf['D_NE_CORE'].attrs["UNITS"] = "cm**-3"
        cdf['D_NE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['D_NE_CORE'].attrs.new('VALIDMAX', data=1e3, type=pycdf.const.CDF_REAL4)
        cdf['D_NE_CORE'].attrs["VAR_TYPE"] = "support_data"
        cdf['D_NE_CORE'].attrs["SCALETYP"] = "linear"
        cdf['D_NE_CORE'].attrs.new('SCALEMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['D_NE_CORE'].attrs.new('SCALEMAX', data=100, type=pycdf.const.CDF_REAL4)
        cdf['D_NE_CORE'].attrs["SI_CONVERSION"] = "1.0e-6>m**-3"
        cdf['D_NE_CORE'].attrs["UCD"] = "stat.error;phys.density;phys.electron"

        # Writing D_TE_CORE variable

        cdf.new('D_TE_CORE', data=self.data['DELTA_TE_CORE_EV_CLEANED_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['D_TE_CORE'].attrs["CATDESC"] = "Uncertainty of electron temperature"
        cdf['D_TE_CORE'].attrs["DEPEND_0"] = "EPOCH"
        cdf['D_TE_CORE'].attrs["DICT_KEY"] = "uncertainty>temperature"
        cdf['D_TE_CORE'].attrs["DISPLAY_TYPE"] = "time_series"
        cdf['D_TE_CORE'].attrs["FIELDNAM"] = "D_TE_CORE"
        cdf['D_TE_CORE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['D_TE_CORE'].attrs['FORMAT'] = "E12.2"
        cdf['D_TE_CORE'].attrs["LABLAXIS"] = "Error on electron temperature"
        cdf['D_TE_CORE'].attrs["UNITS"] = "eV"
        cdf['D_TE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['D_TE_CORE'].attrs.new('VALIDMAX', data=1e6, type=pycdf.const.CDF_REAL4)
        cdf['D_TE_CORE'].attrs["VAR_TYPE"] = "support_data"
        cdf['D_TE_CORE'].attrs["SCALETYP"] = "linear"
        cdf['D_TE_CORE'].attrs.new('SCALEMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['D_TE_CORE'].attrs.new('SCALEMAX', data=100, type=pycdf.const.CDF_REAL4)
        cdf['D_TE_CORE'].attrs["SI_CONVERSION"] = "11604.5250061657>K"
        cdf['D_TE_CORE'].attrs["UCD"] = "stat.error;phys.temperature;phys.electron"

        # Writing QF_TE_CORE variable

        cdf.new('QF_TE_CORE', data=self.data['QUALITY_FLAG_TE_ORBIT'], type=pycdf.const.CDF_INT1,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['QF_TE_CORE'].attrs["CATDESC"] = "Quality Flag for core electron temperature"
        cdf['QF_TE_CORE'].attrs["DEPEND_0"] = "EPOCH"
        cdf['QF_TE_CORE'].attrs["DICT_KEY"] = "flag>quality"
        cdf['QF_TE_CORE'].attrs["DISPLAY_TYPE"] = "time_series"
        cdf['QF_TE_CORE'].attrs["FIELDNAM"] = "QF_TE_CORE"
        cdf['QF_TE_CORE'].attrs.new('FILLVAL', data=-128, type=pycdf.const.CDF_INT1)
        cdf['QF_TE_CORE'].attrs['FORMAT'] = "I4"
        cdf['QF_TE_CORE'].attrs["LABLAXIS"] = "Quality Flag for core electron temperature"
        cdf['QF_TE_CORE'].attrs["UNITS"] = " "
        cdf['QF_TE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_INT1)
        cdf['QF_TE_CORE'].attrs.new('VALIDMAX', data=2, type=pycdf.const.CDF_INT1)
        cdf['QF_TE_CORE'].attrs["VAR_TYPE"] = "support_data"
        cdf['QF_TE_CORE'].attrs["SCALETYP"] = "linear"
        cdf['QF_TE_CORE'].attrs.new('SCALEMIN', data=-1, type=pycdf.const.CDF_INT1)
        cdf['QF_TE_CORE'].attrs.new('SCALEMAX', data=3, type=pycdf.const.CDF_INT1)
        cdf['QF_TE_CORE'].attrs["UCD"] = "meta.code;phys.temperature;phys.electron"

        # Writing R_DIST variable

        cdf.new('R_DIST', data=self.data['R_EPHEM_NSWEEP_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['R_DIST'].attrs['CATDESC'] = "Radial Distance of Cassini to Saturn's Centre"
        cdf['R_DIST'].attrs['DEPEND_0'] = "EPOCH"
        cdf['R_DIST'].attrs['DICT_KEY'] = "position>radial_distance"
        cdf['R_DIST'].attrs['FIELDNAM'] = "R_DIST"
        cdf['R_DIST'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['R_DIST'].attrs['FORMAT'] = "E12.2"
        cdf['R_DIST'].attrs['LABLAXIS'] = "Distance"
        cdf['R_DIST'].attrs['UNITS'] = "Rs"
        cdf['R_DIST'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['R_DIST'].attrs.new('VALIDMAX', data=1000, type=pycdf.const.CDF_REAL4)
        cdf['R_DIST'].attrs['VAR_TYPE'] = "support_data"
        cdf['R_DIST'].attrs['SCALETYP'] = "linear"
        cdf['R_DIST'].attrs.new('SCALEMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['R_DIST'].attrs.new('SCALEMAX', data=10, type=pycdf.const.CDF_REAL4)
        cdf['R_DIST'].attrs["SI_CONVERSION"] = "60.268e6>m"
        cdf['R_DIST'].attrs["UCD"] = "pos.distance;pos.bodyrc"

        # Writing LATITUDE variable

        cdf.new('LATITUDE', data=self.data['LAT_EPHEM_NSWEEP_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['LATITUDE'].attrs['CATDESC'] = "Latitude of Cassini"
        cdf['LATITUDE'].attrs['DEPEND_0'] = "EPOCH"
        cdf['LATITUDE'].attrs['DICT_KEY'] = "position>latitude"
        cdf['LATITUDE'].attrs['FIELDNAM'] = "LATITUDE"
        cdf['LATITUDE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['LATITUDE'].attrs['FORMAT'] = "E12.2"
        cdf['LATITUDE'].attrs['LABLAXIS'] = "Latitude"
        cdf['LATITUDE'].attrs['UNITS'] = "deg"
        cdf['LATITUDE'].attrs.new('VALIDMIN', data=-90, type=pycdf.const.CDF_REAL4)
        cdf['LATITUDE'].attrs.new('VALIDMAX', data=90, type=pycdf.const.CDF_REAL4)
        cdf['LATITUDE'].attrs['VAR_TYPE'] = "support_data"
        cdf['LATITUDE'].attrs['SCALETYP'] = "linear"
        cdf['LATITUDE'].attrs.new('SCALEMIN', data=-90, type=pycdf.const.CDF_REAL4)
        cdf['LATITUDE'].attrs.new('SCALEMAX', data=90, type=pycdf.const.CDF_REAL4)
        cdf['LATITUDE'].attrs["UCD"] = "pos.bodyrc.lat"

        # Writing LOCAL_TIME variable

        cdf.new('LOCAL_TIME', data=self.data['LT_EPHEM_NSWEEP_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['LOCAL_TIME'].attrs['CATDESC'] = "Local time of Cassini"
        cdf['LOCAL_TIME'].attrs['DEPEND_0'] = "EPOCH"
        cdf['LOCAL_TIME'].attrs['DICT_KEY'] = "position>longitude"
        cdf['LOCAL_TIME'].attrs['FIELDNAM'] = "LOCAL_TIME"
        cdf['LOCAL_TIME'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['LOCAL_TIME'].attrs['FORMAT'] = "E12.2"
        cdf['LOCAL_TIME'].attrs['LABLAXIS'] = "Local Time"
        cdf['LOCAL_TIME'].attrs['UNITS'] = "hr"
        cdf['LOCAL_TIME'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['LOCAL_TIME'].attrs.new('VALIDMAX', data=24, type=pycdf.const.CDF_REAL4)
        cdf['LOCAL_TIME'].attrs['VAR_TYPE'] = "support_data"
        cdf['LOCAL_TIME'].attrs['SCALETYP'] = "linear"
        cdf['LOCAL_TIME'].attrs.new('SCALEMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['LOCAL_TIME'].attrs.new('SCALEMAX', data=24, type=pycdf.const.CDF_REAL4)
        cdf['LOCAL_TIME'].attrs["UCD"] = "pos.bodyrc.long"

        # Writing L_SHELL variable

        cdf.new('L_SHELL', data=self.data['LSHELL_EPHEM_NSWEEP_ORBIT'], type=pycdf.const.CDF_REAL4,
                compress=pycdf.const.NO_COMPRESSION)
        cdf['L_SHELL'].attrs['CATDESC'] = "L-Shell of Cassini"
        cdf['L_SHELL'].attrs['DEPEND_0'] = "EPOCH"
        cdf['L_SHELL'].attrs['DICT_KEY'] = "position>radial_distance"
        cdf['L_SHELL'].attrs['FIELDNAM'] = "L_SHELL"
        cdf['L_SHELL'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)
        cdf['L_SHELL'].attrs['FORMAT'] = "E12.2"
        cdf['L_SHELL'].attrs['LABLAXIS'] = "L-Shell"
        cdf['L_SHELL'].attrs['UNITS'] = "Rs"
        cdf['L_SHELL'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)
        cdf['L_SHELL'].attrs.new('VALIDMAX', data=1000, type=pycdf.const.CDF_REAL4)
        cdf['L_SHELL'].attrs['VAR_TYPE'] = "support_data"
        cdf['L_SHELL'].attrs['SCALETYP'] = "linear"
        cdf['L_SHELL'].attrs.new('SCALEMIN', data=1, type=pycdf.const.CDF_REAL4)
        cdf['L_SHELL'].attrs.new('SCALEMAX', data=10, type=pycdf.const.CDF_REAL4)
        cdf['L_SHELL'].attrs["SI_CONVERSION"] = "60.268e6>m"
        cdf['L_SHELL'].attrs["UCD"] = "pos.distance;pos.bodyrc;phys.magField"

        cdf.close()

        return "\n===== FILE {} CREATED =====\nTIMESTAMP = {}\n".\
            format(str(cdf_file.name), datetime.datetime.now().isoformat())

    def _fix_cdf(self):

        cdf_file = _DATA_CDF_DIR / self._cdf_name()
        cdf_temp = Path('/tmp') / cdf_file.name

        cur_command = [str(_CDFLIB_BIN / "cdfconvert"), str(cdf_file), str(cdf_temp)]
        # using subprocess instead of os.system to catch stdout
        cur_p = subprocess.Popen(cur_command, stdout=subprocess.PIPE)
        log_str = cur_p.communicate()[0].decode('ascii')

        cur_command = "mv {} {}".format(str(cdf_temp), str(cdf_file))
        os.system(cur_command)
        log_str += '\nMoving converted "{}" back to "{}"\n'.format(str(cdf_temp), str(cdf_file))

        return log_str

    def check_cdf(self, verbose=False):

        log_str = self._fix_cdf()
        cdf_file = _DATA_CDF_DIR / self._cdf_name()

        verb_flag = ""
        if verbose:
            verb_flag = '-v'

        cur_command = [str(_CDFCHECK_BIN), verb_flag, str(cdf_file)]
        # using subprocess instead of os.system to catch stdout
        cur_p = subprocess.Popen(cur_command, stdout=subprocess.PIPE)
        log_str += cur_p.communicate()[0].decode('ascii')

        cur_command =['java', '-cp',
                      '{}:{}/../cdfjava/classes/cdfjava.jar'.format(str(_SKTEDITOR_BIN), str(_CDFLIB_BIN)),
                      'gsfc.spdf.istp.tools.CDFCheck', str(cdf_file)]
        # using subprocess instead of os.system to catch stdout
        cur_p = subprocess.Popen(cur_command, stdout=subprocess.PIPE)
        log_str += cur_p.communicate()[0].decode('ascii')

        return log_str


def _md5_hash(fname):
    # from: https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b''):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def _file_len(fname):
    # from https://stackoverflow.com/questions/845058/how-to-get-line-count-cheaply-in-python
    i = 0
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def _get_now_tmp_file():
    tmp_file = Path('/tmp/tmp_now.lst')
    with open(str(tmp_file), 'w') as f:
        f.write('value = {}\n'.format(datetime.datetime.now().date().isoformat()))
    return tmp_file


def _get_info_tmp_file(cur_file):
    info_file = Path('/tmp/info.lst')
    with open(str(info_file), 'w') as f:
        f.write('name = {}\n'.format(str(cur_file.name)))
        f.write('size = {}\n'.format(os.path.getsize(str(cur_file))))
        f.write('date = {}\n'.format(datetime.datetime.fromtimestamp(os.path.getmtime(str(cur_file))).
                                     strftime('%Y-%m-%dT%H:%M:%S')))
        f.write('hash = {}\n'.format(_md5_hash(str(cur_file))))
    return info_file


def _get_info_tmp_inventory_file(cur_file):
    info_file = _get_info_tmp_file(cur_file)
    with open(str(info_file), 'a') as f:
        f.write('recs = {}\n'.format(_file_len(str(cur_file))))
    return info_file


def _fix_crlf(cur_file):
    with open(str(cur_file), 'rb') as f0:
        data = f0.read().decode('ascii')
        new_data = data.replace('\r\n', '\n').replace('\r', '\n').replace('\n', '\r\n')
        if new_data != data:
            with open(str(cur_file), 'wb') as f1:
                f1.write(data.encode('ascii'))


def make_pds4_cdf_file_label(file_path):
    return make_pds4_file_label(file_path, file_type='cdf')


def make_pds4_pdf_file_label(file_path):
    return make_pds4_file_label(file_path, file_type='pdf')


def make_pds4_png_file_label(file_path):
    return make_pds4_file_label(file_path, file_type='png')


def make_pds4_doc_file_label(file_path, cur_type='index'):

    # Making Document Label
    cur_file = file_path
    tmp_file = _get_now_tmp_file()

    if cur_type == 'index':
        _fix_crlf(cur_file)
        info_file = _get_info_tmp_inventory_file(cur_file)
        cur_dir_file = _DOCUMENT_DIR
        vm_file = 'co_rpws_hfr_qtn_v10_document_index.vm'
        cdf_file = get_data_file_list(file_type='cdf')[0]
    else:
        raise ValueError

    cur_command = ' '.join([str(_DOCGEN_BIN), "cdf:{}".format(str(cdf_file)),
                            'now:{}'.format(str(tmp_file)),
                            'info:{}'.format(str(info_file)),
                            'pds4/{}'.format(vm_file), '>',
                            str(cur_dir_file / "{}.xml".format(cur_file.stem))])

    os.system(cur_command)
    return "Running: {}\n".format(cur_command)


def make_pds4_file_label(file_path, file_type='cdf'):

    # Making Data and Browse Labels
    cdf_file = file_path
    tmp_file = _get_now_tmp_file()

    if file_type == 'pdf':
        vm_file = 'co_rpws_hfr_qtn_v10_browse_plot.vm'
        cur_dir_file = _BROWSE_PLOT_DIR
        with pycdf.CDF(str(file_path)) as cur_cdf:
            print("{} {}".format(type(cur_cdf), file_path.name))
            cur_file = cur_dir_file / str(cur_cdf.attrs['file_plot_name'])

    elif file_type == 'png':
        vm_file = 'co_rpws_hfr_qtn_v10_browse_thumbnail.vm'
        cur_dir_file = _BROWSE_THUMBNAIL_DIR
        with pycdf.CDF(str(file_path)) as cur_cdf:
            cur_file = cur_dir_file / str(cur_cdf.attrs['file_thumbnail_name'])

    else:
        cur_file = cdf_file
        cur_dir_file = _DATA_CDF_DIR
        vm_file = 'co_rpws_hfr_qtn_v10_data_cdf.vm'

    info_file = _get_info_tmp_file(cur_file)

    cur_command = ' '.join([str(_DOCGEN_BIN), "cdf:{}".format(str(cdf_file)),
                            'now:{}'.format(str(tmp_file)),
                            'info:{}'.format(str(info_file)),
                            'pds4/{}'.format(vm_file), '>',
                            str(cur_dir_file / "{}.xml".format(cur_file.stem))])

    os.system(cur_command)
    return "Running: {}\n".format(cur_command)


def make_pds4_bundle_labels(first_cdf, last_cdf):

    log_tmp = "\n"
    # Making Bundle Label
    readme_file = _BUNDLE_DIR / 'Readme_1.0.txt'
    _fix_crlf(readme_file)
    info_file = _get_info_tmp_file(readme_file)
    tmp_file = _get_now_tmp_file()

    cur_command = ' '.join([str(_DOCGEN_BIN), "cdf0:{}".format(str(first_cdf)), 'cdf1:{}'.format(str(last_cdf)),
                            'now:{}'.format(str(tmp_file)),
                            'info:{}'.format(str(info_file)),
                            'pds4/co_rpws_hfr_qtn_v10_bundle.vm', '>',
                            str(_BUNDLE_DIR / 'bundle_co_rpws_hfr_qtn.xml')])
    os.system(cur_command)
    log_tmp += "Running: {}\n".format(cur_command)

    # Making Data Collection Label
    inventory_file = _DATA_COLL_INVENTORY_FILE
    info_file = _get_info_tmp_inventory_file(inventory_file)
    tmp_file = _get_now_tmp_file()

    cur_command = ' '.join([str(_DOCGEN_BIN), "cdf0:{}".format(str(first_cdf)), 'cdf1:{}'.format(str(last_cdf)),
                            'now:{}'.format(str(tmp_file)),
                            'info:{}'.format(str(info_file)),
                            'pds4/co_rpws_hfr_qtn_v10_data_collection.vm', '>',
                            str(_DATA_COLL_DIR / '{}.xml'.format(inventory_file.stem))])
    os.system(cur_command)
    log_tmp += "Running: {}\n".format(cur_command)

    # Making Browse Collection Label
    inventory_file = _BROWSE_COLL_INVENTORY_FILE
    info_file = _get_info_tmp_inventory_file(inventory_file)
    tmp_file = _get_now_tmp_file()

    cur_command = ' '.join([str(_DOCGEN_BIN), "cdf0:{}".format(str(first_cdf)), 'cdf1:{}'.format(str(last_cdf)),
                            'now:{}'.format(str(tmp_file)),
                            'info:{}'.format(str(info_file)),
                            'pds4/co_rpws_hfr_qtn_v10_browse_collection.vm', '>',
                            str(_BROWSE_COLL_DIR / '{}.xml'.format(inventory_file.stem))])
    os.system(cur_command)
    log_tmp += "Running: {}\n".format(cur_command)

    # Making Document Collection Label
    inventory_file = _DOCUMENT_COLL_INVENTORY_FILE
    info_file = _get_info_tmp_inventory_file(inventory_file)
    tmp_file = _get_now_tmp_file()

    cur_command = ' '.join([str(_DOCGEN_BIN), "cdf0:{}".format(str(first_cdf)), 'cdf1:{}'.format(str(last_cdf)),
                            'now:{}'.format(str(tmp_file)),
                            'info:{}'.format(str(info_file)),
                            'pds4/co_rpws_hfr_qtn_v10_document_collection.vm', '>',
                            str(_DOCUMENT_COLL_DIR / '{}.xml'.format(inventory_file.stem))])
    os.system(cur_command)
    log_tmp += "Running: {}\n".format(cur_command)

    return log_tmp


if __name__ == '__main__':
    argv = sys.argv[1:]

    if '--help' in argv:

        print("This script computes the Cassini/RPWS/HFR/QTN PDS4 archive bundle.\n\n"
              "Requires:\n"
              "    Python 3.5+\n\n"
              "Usage:\n"
              "    python3 qtn.py --init\n"
              "        Downloads raw IDL files and precomputed previews and thumbnails.\n"
              "    python3 qtn.py --cdf\n"
              "        Computes CDF files from original SAV files.\n"
              "    python3 qtn.py --labels\n"
              "        Computes PDS4 XML label files for data, browse and document products.\n"
              "    python3 qtn.py --inventory\n"
              "        Computes inventory files for all collections.\n"
              "    python3 qtn.py --bundle\n"
              "        Computes PDS4 XML labels for collections and bundle\n"
              "    python3 qtn.py --validate\n"
              "        Checks the bundle against the PDS4 validate tool\n"
              "    python3 qtn.py --zip\n"
              "        Computes the zipped bundle archive, and do so mac gremlins clean up\n"
              "NB: The 5 options must be used in the order provided above.\n\n"
              "Logging:\n"
              "    Processing logs are provided in files in the parent directory of the bundle directory.\n\n"
              "B. Cecconi, LESIA, Obs Paris. 2018.")

    # --cdf option computes the CDF files from the IDL SAV files.
    if '--init' in argv:
        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-init.txt"), 'w') as file_process_log:
            file_process_log.write(import_initial_data())

    if '--cdf' in argv:
        check_dirs()
        files = get_data_file_list(file_type='sav')
        intervals = list()

        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-cdf.txt"), 'w') as file_process_log:
            for item in files:
                o = QTNData(item)
                intervals.append({'rev': o.rev_id, 'ndata': len(o),
                                  'time_start': o.start_time.isoformat(),
                                  'time_stop': o.end_time.isoformat()})
                file_process_log.write(o.make_cdf())
                file_process_log.write(o.check_cdf(verbose=True))

            index_file = _DOCUMENT_DIR / "index_1.0.csv"
            with open(str(index_file), 'w') as file_intervals:
                file_intervals.write("REV,NDATA,TIME START,TIME STOP\r\n")
                intervals = sorted(intervals, key=lambda k: k['time_start'])  # sorting on 'time_start' values
                for item in intervals:
                    file_intervals.write("{},{},{},{}\r\n".format(item['rev'], item['ndata'],
                                                                  item['time_start'], item['time_stop']))
                file_process_log.write("\nWriting index_1.0.csv file.\n")

    # add --spase option to build SPASE descriptors (Numerical_data, Display_Data) at collection level to be put into
    # Document collection. Use Todd's template example for this.

    # --labels option computes the PDS4 XML labels for CDF files, browse plots and the index file.
    if '--labels' in argv:
        cdf_list = get_data_file_list(file_type='cdf')

        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-labels.txt"), 'w') as file_process_log:
            for item in cdf_list:
                file_process_log.write(make_pds4_cdf_file_label(item))
                file_process_log.write(make_pds4_pdf_file_label(item))
                file_process_log.write(make_pds4_png_file_label(item))

            file_process_log.write(make_pds4_doc_file_label(_DOCUMENT_DIR / 'index_1.0.csv'))

    # --inventory option computes the PDS4 CSV inventory files for the 3 collections of the bundle
    if '--inventory' in argv:

        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-inventory.txt"), 'w')\
                as file_process_log:

            data_coll_inventory_file = str(_DATA_COLL_INVENTORY_FILE)
            with open(data_coll_inventory_file, 'w') as inventory_data:
                for item in get_data_file_list(file_type='cdf'):
                    with pycdf.CDF(str(item)) as c:
                        print("{} {} {}".format(type(c), item.name, len(c.attrs)))
                        inventory_data.write('P,{}::{}\r\n'.format(str(c.attrs['PDS_LID']),
                                                                   '.'.join(list(_VERSION_CDF))))
            file_process_log.write("\nWriting {}\n".format(data_coll_inventory_file))

            browse_coll_inventory_file = str(_BROWSE_COLL_INVENTORY_FILE)
            with open(browse_coll_inventory_file, 'w') as inventory_browse:
                for item in get_data_file_list(file_type='cdf'):
                    with pycdf.CDF(str(item)) as c:
                        print("{} {}".format(type(c), item.name))
                        inventory_browse.write('P,{}::{}\r\n'.format(str(c.attrs['PDS_LID_plot']),
                                                                     '.'.join(list(_VERSION_CDF))))
                        inventory_browse.write('P,{}::{}\r\n'.format(str(c.attrs['PDS_LID_thumbnail']),
                                                                     '.'.join(list(_VERSION_CDF))))
            file_process_log.write("Writing {}\n".format(browse_coll_inventory_file))

            document_coll_inventory_file = str(_DOCUMENT_COLL_INVENTORY_FILE)
            with open(str(document_coll_inventory_file), 'w') as inventory_document:
                inventory_document.write('P,urn:nasa:pds:co-rpws-saturn:hfr-qtn-document:index::1.0\r\n')
            file_process_log.write("Writing {}\n".format(document_coll_inventory_file))

    # --bundle option computes the PDS4 XML collections and bundle labels
    if '--bundle' in argv:

        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-bundle.txt"), 'w') as file_process_log:
            cdf_list = list(get_data_file_list(file_type='cdf'))
            file_process_log.write(make_pds4_bundle_labels(cdf_list[0], cdf_list[-1]))

    # --validate option to check the bundle with PDS4 validate tool
    if "--validate" in argv:
        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-validate.txt"), 'w') as file_process_log:
            command = [str(_VALIDATE_BIN), str(_BUNDLE_DIR)]
            file_process_log.write("Running: {}\n\n".format(' '.join(command)))
            file_process_log.flush()

            subprocess.call(command, stdout=file_process_log, stderr=file_process_log,
                            env={**os.environ, 'JAVA_HOME': str(_JAVA_HOME)})

    # --zip option to compress the bundle into an ZIP archive file and do some clean up
    if '--zip' in argv:
        with open(str(_BUILD_DIR / "co_rpws_hfr_qtn-pds-processing-log-zip.txt"), 'w') as file_process_log:

            command = ['cd', str(_BUILD_DIR), ';', 'zip', '-r', _BUNDLE_NAME, _BUNDLE_NAME]
            file_process_log.write("Running: {}\n\n".format(' '.join(command)))
            file_process_log.flush()

            subprocess.call(" ".join(command), shell=True, stdout=file_process_log, stderr=file_process_log)
            file_process_log.flush()

            file_process_log.write("\n\n")
            file_process_log.flush()

            command = ['cd', str(_BUILD_DIR), ';', 'zip', '-d', _BUNDLE_NAME, '\*/.DS_Store']
            file_process_log.write("Running: {}\n\n".format(' '.join(command)))
            file_process_log.flush()

            subprocess.call(" ".join(command), shell=True, stdout=file_process_log, stderr=file_process_log)
            file_process_log.flush()

    if '--clean' in argv:
        clean()