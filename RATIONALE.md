# Common Data Format (CDF)

## Why CDF?
The CDF file format has been used as an exchange and archive format for decades by the heliophysics and space physics
community. Most of the tools and libraries developed and used in this science fields are including CDF reading and
writing capabilities. CDF is developed and maintained by NASA/GSFC ([more details](https://cdf.gsfc.nasa.gov)).
Furthermore, CDF is now accepted by [NASA/PDS](https://pds.nasa.gov) as a archive format, as well as by
[JAXA/DARTS](https://www.darts.isas.jaxa.jp) and [CNES/CDPP](https://cdpp-archive.cnes.fr).

## What is CDF?
CDF is a format that contains attributes and variables. The attributes can be set at the root level of the CDF
file (_Global Attributes_), or within each variable definition (_Variable Attributes_). The root level of a CDF
file is composed of a series of _Global Attributes_ and a series of _Variables_. There are several types of variables in
the CDF format, but only _zVariables_ are used here. For more information, please refer to the
[full CDF documentation](https://cdf.gsfc.nasa.gov/html/cdf_docs.html).

## CDF conventions and metadata
Several metadata standards are applicable for heliophysics and space physics data products.

The main set of conventions and metadata has been defined within the International Solar Terrestral Program (ISTP).
Those set of metadata and conventions are now commonly used. The CDF-ISTP convention defines a set of [_Global
Attributes_](https://spdf.gsfc.nasa.gov/istp_guide/gattributes.html) some being mandatory and others optional. This
convention also defines how to build the [_Variables_](https://spdf.gsfc.nasa.gov/istp_guide/variables.html) and the
[_Variable Attributes_](https://spdf.gsfc.nasa.gov/istp_guide/vattributes.html). The ISTP group also provides a tool
for building _CDF Skeleton_ files: the [SKTEditor](https://spdf.sci.gsfc.nasa.gov/skteditor/).

A convention for [CDF-A](https://pds-ppi.igpp.ucla.edu/doc/CDF-A-Specification-v1.0.pdf) (Archive quality CDF) has
been defined and must be followed when building a data processing pipeline producing archive data in CDF.

The PPI (Planetary Plasma Interactions) node of NASA/PDS provides a [guidelines for building NASA/PDS compliant CDF
files](https://pds-ppi.igpp.ucla.edu/doc/cdf/Guide-to-Archiving-CDF-Files-in-PDS4-v7.pdf). It basically states to
follow the ISTP and CDF-A conventions. It also requires to remove any data compression feature, and to use only
_zVariables_.

# PDS4 Archive
The NASA/PDS4 archive system is based on XML technologies and implements the Open Archival Information System (OAIS)
Reference Model (ISO 14721). The NASA/PDS/PPI node is proposing [PDS4 training
material](https://pds-ppi.igpp.ucla.edu/doc/pds4-training-exercise-v6.pptx), which is a very good starting point.

In the NASA/PDS4 system, any archive object is called a _Product_. Each _Product_ is composed of an XML label file
and an object (either a file, a collection of files, an abstract concept...) described by the label. _Data Products_
are usually composed of a label file describing a data file. Each _Data Product_ is assigned with a unique pair of
identifiers composed of a logical identifier (LID) and version identifier (VID). A _Collection Product_ is composed of
label and a file containing the list of _Data Product_ unique identifiers. A _Bundle Product_ is composed of a label
file, linking to _Collection Products_.

# SPASE
The [SPASE (Space Physics Archive Search and Extract) group](http://www.spase-group.org) is providing a data model for
space physics, as well as a registry of resources (numerical data, browse files, persons, observatories...).
It is planned to include building SPASE metadata descriptors in the pipeline. 