## Preliminary review of v1.0-r1 (2018-07-13) by Joe Mafi (PDS-PPI)

# XML Schema and Schematron reference => __OK__

Please use this for the Schematron information:

```
<?xml-model href="https://pds.nasa.gov/pds4/pds/v1/PDS4_PDS_1A00.sch" type="application/xml"
    schematypens="http://purl.oclc.org/dsdl/schematron"?>
```

Please use this for the schemaLocation:

```
<Product_Observational xmlns="http://pds.nasa.gov/pds4/pds/v1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://pds.nasa.gov/pds4/pds/v1
    https://pds.nasa.gov/pds4/pds/v1/PDS4_PDS_1A00.xsd">
```

* [x] BC: Update the schema location

# Recommend including your external references in the CDF global attribute TEXT.

# File statistics with docgen

The following is code to calculate the file stats in docgen:
```
    <File>
        <file_name>$options.cdfname</file_name>
        <creation_date_time>$File.getModificationDate("$options.cdfpath/$options.cdfname")</creation_date_time>
        <file_size unit="byte">$File.getSize("$options.cdfpath/$options.cdfname")</file_size>
        <md5_checksum>$File.getMD5("$options.cdfpath/$options.cdfname")</md5_checksum>
    </File>
```

# Reference to L1 dataset

Recommend adding a reference to the Cassini RPWS Raw data set.

* [x] BC: Add reference to Cassini RDR LRFULL dataset


# Inventory files 

Please rename the collection inventory with the same basename as the collection label. Please rename the collection
label with "collection" as the first component of the filename. Please do the same for the bundle label (bundle first).
The Readme file name is fine as is.

* [x] BC: rename collection inventory

# subdirectories

The subdirectories in each of you collection directories are not necessary. The individual files can go into the same 
directory as the collection label and inventory if that works for you.

* [x] BC: Keep directories or not?

==> decision = keep directories

# SKTEditor output

```
Global Attribute "File_naming_convention" was changed from source_descriptor_type_yyyymmddhhmm_yyyymmddhhmm_ver to source_descriptor_type.
Global Attribute "Logical_file_id" was changed from co_rpws_hfr_qtn_000000000000_000000000000_v00 to co_rpws_hfr_qtn__v03 to be consistent with source_descriptor_type.
###############################
Compliance Check for \\128.97.68.67\work1\WORKING\Cassini\RPWS\data\high_order\QTN\Deliveries\180709-archive\co_rpws_saturn_hfr_qtn_bundle_v10\hfr-qtn-data\cdf\co_rpws_hfr_qtn_200501151738_200501161826_v10.cdf
CDF File Version: 3.6.4
File Last Leap Second: 2017-01-01
Majority: Column
\\128.97.68.67\work1\WORKING\Cassini\RPWS\data\high_order\QTN\Deliveries\180709-archive\co_rpws_saturn_hfr_qtn_bundle_v10\hfr-qtn-data\cdf\co_rpws_hfr_qtn_200501151738_200501161826_v10.cdf is not ISTP-Compliant.
Global errors:
	Global attribute VESPA_time_sampling_min is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_time_sampling_max is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_c1min is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_c1max is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_c2min is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_c2max is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_c3min is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	Global attribute VESPA_c3max is of type CDF_FLOAT.
	    Datatypes other than CDF_CHAR may be problematic.
	spase_DatasetResourceID is missing.
The following variables are not ISTP-compliant:
	QF_TE_CORE
		FILLVAL value of '-127.0' is non-standard.
		    The recommended value is '-1.0E31'.
	Epoch
		ISTP epoch variable 'EPOCH' is missing the TIME_BASE attribute.
		The TIME_BASE attribute has been added.
```

* [x] BC: fix errors