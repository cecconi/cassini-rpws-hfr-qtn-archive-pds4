# Installation

The following libraries have to be installed in order to run the current package.

## CDF C-library

The latest version of the CDF library is available from the [CDF latest
release](https://spdf.sci.gsfc.nasa.gov/pub/software/cdf/dist/latest-release/) page. Follow the installation steps as
described in the package, including the last script setting environment variables and paths.

## Python Requirements

This library requires Python 3+.

Install the [SpacePy](https://pythonhosted.org/SpacePy/) and
[SciPy](https://www.scipy.org) packages. The simplest way is to use the `pip`
tool:

```
pip install spacepy
pip install scipy
```

* _NB1_: the `spacepy` package requires some underliying libraries, see the
[SpacePy installation page](https://pythonhosted.org/SpacePy/install.html).
* _NB2_: the `spacepy` package version available on [SpacePy Github repository](https://github.com/spacepy/spacepy)
fixes an intermittent bug. In this case, you should not use pip to install, but rather
download the version from github and follow the installation instructions.


## PDS4 Validation libraries

We propose to place PDS validation libraries into the `lib/` directory of this package. The main script is configured as
such. If you use other paths than those proposed here, modify the [config.ini](config.ini) file. All the following commands
should be ran in the `lib/` directory of this package:
```
cd lib
```

### PDS.CDF: Check the CDF compliance with PDS4
Download the latest version of `pds.cdf` from
[http://release.igpp.ucla.edu/pds/cdf/](http://release.igpp.ucla.edu/pds/cdf/). Unpack the package into the lib/
directory and create a link to that directory (at the time of writing, the version number 1.0.11):
```
wget http://release.igpp.ucla.edu/pds/cdf/pds-cdf-1.0.11-dist.zip
unzip pds-cdf-1.0.11-dist.zip
ln -s pds-cdf-1.0.11 pds-cdf
chmod u+x pds-cdf/bin/cdfcheck
```

### PDS4 validate tool
Download the latest version of `validate` from
[https://github.com/NASA-PDS-Incubator/validate/releases/](https://github.com/NASA-PDS-Incubator/validate/releases/). Unpack the package
into the lib/ directory and create a link to that directory (at the time of writing, the version number
1.17.5):
```
wget https://github.com/NASA-PDS-Incubator/validate/releases/download/v1.17.5/validate-1.17.5-bin.tar.gz
tar xvzf validate-1.17.5-bin.tar.gz
ln -s validate-1.17.5 validate
chmod u+x validate/bin/validate
```

### SKTEditor library
Download the latest version of `skteditor` from
[https://spdf.gsfc.nasa.gov/skteditor/standalone-skteditor-1.3.1.41.zip](https://spdf.gsfc.nasa.gov/skteditor/standalone-skteditor-1.3.1.41.zip).
Unpack the package into the lib/ directory and create a link to that directory (at the time of writing, the version number is 1.3.1.41):
```
wget https://spdf.gsfc.nasa.gov/skteditor/standalone-skteditor-1.3.1.41.zip
unzip standalone-skteditor-1.3.1.41.zip
ln -s skteditor-1.3.1.41 skteditor
```

## Apache Velocity Templating scripts
This tool is used to build the XML labels directly the CDF files, using template files.

Download the latest version of `igpp.docgen` from
[http://release.igpp.ucla.edu/igpp/docgen/](http://release.igpp.ucla.edu/igpp/docgen/). Unpack the package
into the lib/ directory this package and create a link to that directory (at the time of writing, the version number
1.0.22):
```
wget http://release.igpp.ucla.edu/igpp/docgen/igpp-docgen-1.0.22.zip
unzip igpp-docgen-1.0.22.zip
ln -s igpp-docgen-1.0.22 igpp-docgen
chmod u+x igpp-docgen/bin/docgen
```
