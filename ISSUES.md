# Known issues

## ISTP compliance check with SKTEditor library fails
Two errors are reported by the ISTP compliance check with the SKTEditor library, for instance:
```
Errors:
	Logical_file_id should be 'co_rpws_hfr_qtn_200502160509_v03'.  It is 'co_rpws_hfr_qtn_200502160509_200502171439_v10'.
	spase_DatasetResourceID is missing.
```
- The first error is based on an ISTP convention assuming the file name should contain a single date or datetime
(which would be the start datetime in our case). Our convention (`<<start-datetime>>_<<end-datetime>>`) is used
by other projects like Solar-Orbiter.
- The second error is triggered although the `spase_DatasetResourceID` is mentionned as optional in the ISTP CDF web
pages [as recommended](https://spdf.gsfc.nasa.gov/istp_guide/gattributes.html#spase_DatasetResourceID). This attribute is required if the data product is planned to be listed in the [SPASE registry](http://www.spase-group.org)
(under development for this dataset).


## Intermittent spacepy.pycdf.CDF BAD_ENTRY_NUM error during "--labels" stage.
This looks like a [SpacePy bug](https://sourceforge.net/p/spacepy/code/ci/d043ae4074af40bcf9e7bf5364279eaacb805e94/).
I usually wait for a few minutes and try again. A bug report has been
[submitted](https://github.com/spacepy/spacepy/issues/25).

__Fixed__ in latest github version of spacepy.

## Bug in _PDS4 validate tool_ flagging negative floats as errors.
Use the PDS4 validate tool available from https://pds-gamma.jpl.nasa.gov/tools/about/validate/v1/index-1A10.shtml