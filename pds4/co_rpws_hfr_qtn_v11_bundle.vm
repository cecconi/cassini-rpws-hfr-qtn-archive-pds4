<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://pds.jpl.nasa.gov/datastandards/schema/released/pds/v1/PDS4_PDS_1D00.sch"
        type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<Product_Bundle xmlns="http://pds.nasa.gov/pds4/pds/v1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://pds.nasa.gov/pds4/pds/v1 
        https://pds.jpl.nasa.gov/datastandards/schema/released/pds/v1/PDS4_PDS_1D00.xsd">
    <Identification_Area>
        <logical_identifier>urn:nasa:pds:cassini-rpws-hfr-qtn</logical_identifier>
        <version_id>1.1</version_id>
        <title>$cdf0.getAttributeValue('Logical_source_description') Bundle</title>
        <information_model_version>1.13.0.0</information_model_version>
        <product_class>Product_Bundle</product_class>
        <Citation_Information>
            <author_list>Schippers, Patricia; Moncuquet, Michel</author_list>
            <editor_list>Cecconi, Baptiste</editor_list>
            <publication_year>2020</publication_year>
            <description>This bundle contains RPWS-HFR Quasi Thermal Noise (QTN) spectroscopy supplied CDF and plot
                files containing a time series of thermal plasma moments (density and temperature). QTN data products
                are generated only when the QTN analysis is applicable.</description>
        </Citation_Information>
        <Modification_History>
            <Modification_Detail>
                <modification_date>$now.value</modification_date>
                <version_id>1.1</version_id>
                <description>Metadata update</description>
            </Modification_Detail>
            <Modification_Detail>
                <modification_date>2020-02-27</modification_date>
                <version_id>1.0</version_id>
                <description>HFR-QTN Initial Release</description>
            </Modification_Detail>
        </Modification_History>
    </Identification_Area>
    <Context_Area>
        <Time_Coordinates>
            <start_date_time>$cdf0.getAttributeValue('PDS_start_time')</start_date_time>
            <stop_date_time>$cdf1.getAttributeValue('PDS_stop_time')</stop_date_time>
        </Time_Coordinates>
        <Primary_Result_Summary>
            <purpose>Science</purpose>
            <processing_level>Derived</processing_level>
            <Science_Facets>
                <wavelength_range>Radio</wavelength_range>
                <domain>Heliosphere</domain>
                <domain>Magnetosphere</domain>
                <discipline_name>Particles</discipline_name>
                <facet1>Electrons</facet1>
            </Science_Facets>
        </Primary_Result_Summary>
        <Investigation_Area>
            <name>$cdf0.getAttributeValue("Mission_group")</name>
            <type>Mission</type>
            <Internal_Reference>
                <lid_reference>urn:nasa:pds:context:investigation:mission.cassini-huygens</lid_reference>
                <reference_type>bundle_to_investigation</reference_type>
            </Internal_Reference>
        </Investigation_Area>
        <Observing_System>
            <Observing_System_Component>
                <name>Cassini Orbiter</name>
                <type>Spacecraft</type>
                <Internal_Reference>
                    <lid_reference>urn:nasa:pds:context:instrument_host:spacecraft.co</lid_reference>
                    <reference_type>is_instrument_host</reference_type>
                </Internal_Reference>
            </Observing_System_Component>
            <Observing_System_Component>
                <name>RPWS</name>
                <type>Instrument</type>
                <Internal_Reference>
                    <lid_reference>urn:nasa:pds:context:instrument:rpws.co</lid_reference>
                    <reference_type>is_instrument</reference_type>
                </Internal_Reference>
            </Observing_System_Component>
        </Observing_System>
        <Target_Identification>
            <name>Saturn</name>
            <type>Planet</type>
            <Internal_Reference>
                <lid_reference>urn:nasa:pds:context:target:planet.saturn</lid_reference>
                <reference_type>bundle_to_target</reference_type>
            </Internal_Reference>
        </Target_Identification>
    </Context_Area>
    <Reference_List/>
    <Bundle>
        <bundle_type>Archive</bundle_type>
        <description>Cassini RPWS QTN Data Bundle</description>
    </Bundle>
    <File_Area_Text>
        <File>
            <file_name>Readme_1.0.txt</file_name>
            <creation_date_time>$info.date</creation_date_time>
            <md5_checksum>$info.hash</md5_checksum>
            <comment>This file contains an overview of the QTN dataset and structure.</comment>
        </File>
        <Stream_Text>
            <name>Readme_1.0.txt</name>
            <offset unit="byte">0</offset>
            <object_length unit="byte">$info.size</object_length>
            <parsing_standard_id>7-Bit ASCII Text</parsing_standard_id>
            <description>This file contains an overview of the QTN dataset and structure.</description>
            <record_delimiter>Carriage-Return Line-Feed</record_delimiter>
        </Stream_Text>
    </File_Area_Text>
    <Bundle_Member_Entry>
        <lidvid_reference>urn:nasa:pds:co-rpws-saturn:hfr-qtn-data::1.1</lidvid_reference>
        <member_status>Primary</member_status>
        <reference_type>bundle_has_data_collection</reference_type>
    </Bundle_Member_Entry>
    <Bundle_Member_Entry>
        <lidvid_reference>urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse::1.1</lidvid_reference>
        <member_status>Primary</member_status>
        <reference_type>bundle_has_browse_collection</reference_type>
    </Bundle_Member_Entry>
    <Bundle_Member_Entry>
        <lidvid_reference>urn:nasa:pds:co-rpws-saturn:hfr-qtn-document::1.1</lidvid_reference>
        <member_status>Primary</member_status>
        <reference_type>bundle_has_document_collection</reference_type>
    </Bundle_Member_Entry>
</Product_Bundle>






