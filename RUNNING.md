# Running the demo package

For each step, a log file will be placed in the [build](build) directory.

## Loading the input files
```
python3 qtn.py --init
```
This step downloads the input IDL saveset files, as well as precomputed
graphical files (PDF summary plots and PNG thumbnails). The files will
be placed into the [build](build) directory. The directory structure
required for the data package production is also setup at this stage.

## Create the CDF files
```
python3 qtn.py --cdf
```
This step converts the IDL saveset files into CDF. An index file is also
built during this step. Each CDF file is validated using several
validation tools (ISTP and PDS4 compliance, as required by the [PDS4
archiving guide](https://pds-ppi.igpp.ucla.edu/doc/cdf/Guide-to-Archiving-CDF-Files-in-PDS4-v7.pdf))

## Create the XML labels
```
python3 qtn.py --labels
```
The XML labels are built upon the CDF global attribute entries.
Each product has its own XML label. In this step, we build the
labels for the CDF, PDF and PNG files.

## Create the CSV inventory files
```
python3 qtn.py --inventory
```
The inventory files are listing the PDS logical identifiers (LID)
for each product.

## Create the bundle
```
python3 qtn.py --bundle
```
In this step, we produce the data collection and bundle labels.

## Validate the bundle
```
python3 qtn.py --validate
```
The PDS validator tool is run on the bundle.

## Create a ZIP archive delivery package
```
python3 qtn.py --zip
```
A zipped archive file is built (removing mac os gremlins files if necessary).

## Cleaning the build directory
```
python3 qtn.py --clean
```
This erases the build directory.