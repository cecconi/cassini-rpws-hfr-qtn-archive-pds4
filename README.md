The package contains scripts to build the Cassini/RPWS/HFR/QTN PDS4 data archive 
Bundle. The data is converted into [CDF](https://cdf.gsfc.nasa.gov) (Common Data
Format), a standard data format in space physics. The CDF data products are 
compliant with the [ISTP](https://spdf.gsfc.nasa.gov/istp_guide/istp_guide.html)
(International Solar-Terrestrial Program), 
[PDS4](https://pds.nasa.gov/datastandards/about/) 
(Planetary Data System version 4) and 
[VESPA](http://www.europlanet-vespa.eu/EPN2020.shtml) (Virtual European Solar 
and Planetary Access) metadata standards. The data produced by this package is 
also compliant with the [CDPP](http://cdpp.eu) archive constraints.

# Getting Started

## Installation
For an installation guide, check the [INSTALL](INSTALL.md) file.

## Command Line
The description of the command line interface is available in the 
[RUNNING](RUNNING.md) file.

# Rationale

This package is a demonstrator developed in the frame of 
[MASER](http://maser.lesia.obspm.fr) (Measurements, Analysis, Simulation of 
Emissions in the Radio range). It shows how to prepare a data bundle 
for the NASA/PDS4 archive in CDF format.

For more details on the formats and metadata, see [RATIONALE](RATIONALE.md).