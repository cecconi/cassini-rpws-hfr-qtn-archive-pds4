 
   DATA_SET_TERSE_DESC
    
      The Cassini Radio and Plasma Wave Science (RPWS) Quasi Thermal
      Noise (QTN) data set contains thermal plasma parameters derived
      by QTN spectroscopy analysis applied on data acquired with the 
      High Frequency Receiver (HFR) during the perikrones of the 
      entire mission.


    ABSTRACT_DESC

      The Cassini Radio and Plasma Wave Science (RPWS) Quasi Thermal
      Noise (QTN) data set contains thermal plasma parameters derived
      by QTN spectroscopy analysis applied on data acquired with the
      High Frequency Receiver (HFR) during the perikrones of the
      entire mission. This data set includes the total electron density 
      and the core electron temperature, as well as the uncertainties on 
      the measurements. It also includes ancillary data about the location 
      of the spacecraft at the time of the measurement: distance to Saturn, 
      local-time, kronographic latitude and dipolar L-Shell apex distance. 
      Data are presented in CDF files, which contain a 1D array depending 
      on time for each parameter. This data set is intended to be the most 
      comprehensive and complete data set for thermal plasma parameters 
      measured by the HFR in the Cassini RPWS archive. A browse data set 
      is included with these data which provides for a graphical search 
      of the data using a series of thumbnail and full-sized plots which 
      lead the user to the particular data file(s) of interest.


    Identification

    Product               LID
      data collection      urn:nasa:pds:co-rpws-saturn:hfr-qtn-data
      data product         urn:nasa:pds:co-rpws-saturn:hfr-qtn-data:200406301642-200407010951-cdf
                           urn:nasa:pds:co-rpws-saturn:hfr-qtn-data:200410280008-200410282357-cdf
                           urn:nasa:pds:co-rpws-saturn:hfr-qtn-data:YYYYMMDDhhmm-YYYYMMDDhhmm-cdf
      browse collection    urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse
      browse product       urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:200406301642-200407010951-plot
                           urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:200410280008-200410282357-plot
                           urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:YYYYMMDDhhmm-YYYYMMDDhhmm-plot
      thumbnail product    urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:200406301642-200407010951-thumbnail
                           urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:200410280008-200410282357-thumbnail
                           urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:YYYYMMDDhhmm-YYYYMMDDhhmm-thumbnail
      document collection  urn:nasa:pds:co-rpws-saturn:hfr-qtn-document
      index product        urn:nasa:pds:co-rpws-saturn:hfr-qtn-document:index
    
    data version = 1.0
    software version = 1.0
    
    
    CITATION_DESC

  Schippers, P., M. Moncuquet, N. Meyer-Vernet, and A. Lecacheux (2013), 
    "Core electron temperature and density in the innermost Saturn's 
    magnetosphere from HF power spectra analysis on Cassini." J. Geophys. 
    Res. Space Physics, 118, 7170-7180.
    10.1002/2013JA019199

  Moncuquet, Michel, et al. 2005. "Quasi Thermal Noise 
    Spectroscopy in the Inner Magnetosphere of Saturn with 
    Cassini/RPWS: Electron Temperatures and Density." Geophys. 
    Res. Lett. 32: L20S02.
    10.1029/2005GL022508

  Kurth, W.S., R.A. Johnson, and L.J.
      Granroth, CASSINI V/E/J/S/SS RPWS CALIBRATED LOW RATE FULL RES
      V1.0, CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0, NASA Planetary Data
      System, 2004.


    DATA_SET_DESC

  Data Set Overview
  =================
    The Cassini Radio and Plasma Wave Science (RPWS) Quasi Thermal
    Noise (QTN) data set contains thermal plasma parameters derived
    by QTN spectroscopy analysis applied on data acquired with the
    High Frequency Receiver (HFR) during the perikrones of the
    entire mission. This data set includes the total electron density
    and the core electron temperature, as well as the uncertainties on 
    the measurements. It also includes ancillary data about the location 
    of the spacecraft at the time of the measurement: distance to Saturn, 
    local-time, kronographic latitude and dipolar L-Shell apex distance. 
    Data are presented in CDF files, which contain a 1D array depending 
    on time for each parameter. This data set is intended to be the most 
    comprehensive and complete data set for thermal plasma parameters 
    measured by the HFR in the Cassini RPWS archive. A browse data set 
    is included with these data which provides for a graphical search 
    of the data using a series of thumbnail and full-sized plots which 
    lead the user to the particular data file(s) of interest.

  Parameters
  ==========
    This data set comprises spacecraft event time, radial distance (in 
    Saturn Radii) from Saturn, latitude (in deg), local time (in hours) 
    and L-shell (in Saturn Radii) of the spacecraft, total electron number 
    density (in cm-3) with the measurement uncertainty, core electron 
    temperature (in eV) with the measurement uncertainty and quality flag
    that were acquired by QTN analysis on RPWS/HFR Spectra.

  Processing
  ==========
    The present data set was derived from the level 2 data of RPWS/HFR
    [1]. The analysis was compiled by the LESIA team (Observatoire de
    Paris-CNRS-PSL, Meudon, France) [2].
    
    The total electron density is deduced from a strong signal peak near 
    the upper-hybrid resonance (FuH), independently of any calibration. 
    Indeed, the plasma frequency Fp can be derived from the FuH resonance 
    and the gyrofrequency Fg (derived from magnetic field measurements by 
    the Cassini/MAG instrument [3]):

         /    2    2 \ 1/2
    Fp = | FuH - Fg  | 
         \           /
    
    The total electron density Ne is then obtained as:
    
                        / 2 pi \ 2    2   
    Ne = Epsilon_0 m_e  | ---- |    Fp
                        \  e   /
  
    The error on the density was calculated on the basis of the uncertainty of
    the HFR receiver spectral relative resolution (df/f = 5%, 10% or 20%). 
    
    The core electron temperature is deduced from the thermal plateau
    level Vmin^2 below FuH, given in [4] as a function of the core
    temperature Tc and Debye length LD (see Eq. 1 of [4]):
    
                                    1/2       / oo               2    
        2           ( 2 m_e k_B Tc )          |     F_V(kL) k L_D
    Vmin    ~ 8 ----------------------------  |    ---------------  dk
                  3/2                      2  |    /  2    2     \2
                pi    Epsilon_0 (1+C_B/C_A)   / O  | k  L_D  + 1 |
                                                   \             /
    
    (S.I. units), Vmin^2 in V^2/Hz. Here F_V(kL) is the Cassini 
    V-shaped antenna response, with L the single wire length (L ~ 10m), 
    
          pi Epsilon_0 L
    C_A= ----------------
            ln(L_D / a)

    is the dipole antenna capacitance at low frequencies, with a the 
    wire radius (a ~1.4 cm), and C_B is the base capacitance. We then 
    use an iterative method to deduce T_c.

    The error on the temperature is determined a posteriori by 
    estimating the averaged 1-sigma dispersion of the temperature level 
    during the perikrone.

    References:
    
    [1] Kurth, W.S., R.A. Johnson, and L.J. Granroth, CASSINI V/E/J/S/SS
        RPWS CALIBRATED LOW RATE FULL RES V1.0,
        CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0, NASA Planetary Data System,
        2004.
    [2] Schippers, P., M. Moncuquet, N. Meyer-Vernet, and A. Lecacheux (2013),
        "Core electron temperature and density in the innermost Saturn's 
        magnetosphere from HF power spectra analysis on Cassini." J. Geophys. 
        Res. Space Physics, 118, 7170-7180, doi:10.1002/2013JA019199.
    [3] Dougherty, Michele, et al. 2004. "The Cassini Magnetic Field
        Investigation", Space. Sci. Rev. 114: 331-383.

    [4] Moncuquet, Michel, et al. 2005. "Quasi Thermal Noise
        Spectroscopy in the Inner Magnetosphere of Saturn with
        Cassini/RPWS: Electron Temperatures and Density." Geophys.
        Res. Lett. 32: L20S02. doi:10.1029/2005GL022508.


  Data
  ====
    The RPWS HFR QTN data set in included in CDF files. Each parameter
    is a 1D variable in the file, depending on time. The SCET of each 
    sample corresponds to the beginning of the HFR sweep. Hence, the real 
    measurement time can be up to about 2 s after that time, depending 
    on the operating mode and on the value of FuH.


  Ancillary Data
  ==============
    Ancillary data are included in the files. They provide the location 
    of the spacecraft at the time of the measurement (distance to Saturn,
    latitude, local time and L-Shell).


  Coordinate System
  =================
    The data in this data set are thermal plasma parameters derived from
    HFR spectra using the thermal noise spectroscopy theory. These 
    parameters are presented as detected by the sensor and are not
    rotated into any other coordinate system. If desired the SPICE
    kernels can be used with the SPICE toolkit to convert from the
    spacecraft frame to virtually any frame which may be of use in
    analyzing these data.  However, for most purposes, the parameters
    may be considered point measurements at Cassini and may be entirely
    adequate with no coordinate transformations at all.


  Media/Format
  ============
    These data are supplied to the Planetary Data System as an online
    collection of CDF files.


    CONFIDENCE_LEVEL_NOTE


  Confidence Level Overview
  =========================
    The data in this data set are thermal plasma parameters derived from
    HFR spectra using the thermal noise spectroscopy theory. Every data 
    has been reviewed by a scientist to ensure that all data included in
    this data set is scientifically consistent. When the data can not be 
    provided during the selected intervals, the values are replaced by 
    fill values (FillVal = -1e+31).


  Review
  ======
    The RPWS HFR QTN data set has been internally reviewed by the Cassini 
    RPWS team prior to release to the PDS. The data set has also been 
    peer reviewed by the PDS.


  Data Coverage and Quality
  =========================
    All data in the stated interval are included, to the best of our
    knowledge and attempts to determine completeness.  This initial
    release is consisting of results for each perikrone from the SOI 
    (Saturn Orbit Insertion) to April 2012. The time interval for 
    each data file is in the index_1.0.csv file (in the 
    hfr-qtn-document collection), with the associated revolution 
    number (i.e. orbit number) and the number of data points.


  Limitations
  ===========
    The Quasi Thermal Noise Spectroscopy analysis is using the FuH 
    resonance for the determination of the electron core density. This 
    determination depends on: (1) the ability to identify the resonance 
    among the other lines and noises present in the data at the same 
    time; (2) the operating spectral resolution, which defines the
    accuracy the the determination of FuH. Concerning the first item,
    the scientific evaluation step is ensuring that the detected FuH
    line is not another emission or a spurious detection. 
    
    The electron core temperature is dependent on the thermal plateau 
    level below the Upper hybrid resonance peak. The accuracy of the 
    temperature determination depends on the integration time (the 
    longer reducing the statistical noise on the data). The core electron 
    temperature determination is also highly dependent on the shot noise 
    contamination induced by the impact of the charged particles on the 
    antennas (proportional to f^-2). 
    
    The shot noise is dominant in the lowest part of the HFR frequency 
    band and may artificially increase the minimum QTN level, inducing
    an overestimate of the core temperature. For this reason, we rejected 
    the spectra where the contribution of the shot noise is larger than 
    20% of the thermal plateau level below FuH in the provided dataset. 
    The Quality Flag on temperature QF is equal to 1 when the shot noise 
    ratio is lower than 10%, and 0 when the ratio is comprised between 10% 
    and 20%. For best quality data, we recommend to the user to use the 
    data with QF=1. Note that the estimate of the core temperature is 
    dependent on the calibration in V^2/Hz of the RPWS/HFR receiver.
    Calibration may evolve with time and influence the thermal plateau 
    level.   

    The value of the temperature finally also depends on the absolute 
    flux calibration of the HFR, and thus may change in the future in 
    when a new calibration is available.
